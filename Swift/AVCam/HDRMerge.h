//
//  HDRMerge.h
//  AVCam
//
//  Created by a on 25/06/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

//#ifdef __cplusplus
//template <typename pixelT>
typedef struct HDRImage {
	float * data;
	int width;
	int height;
	int channels;
	
	//HDRImage(const float * data, const int width, const int height, const int channels) : data(data), width(width), height(height), channels(channels) {}
	
	/*Image(const cv::Mat & image) {
		// TODO
		//cv::imread("");
		//image.
	}*/
} HDRImage;

typedef struct ImageStats {
	double min;
	double max;
} ImageStats;

//typedef Image<uint8_t> LDRImage;
//typedef Image<float> HDRImage;
/*#else
class HDRImage;
#endif*/

@interface HDRMerge : NSObject

@property (nonatomic, assign) unsigned int captureCounter;
-(instancetype)init NS_UNAVAILABLE;
-(id) init:(unsigned int)captureCounter;
-(ImageStats) measureImage:(CMSampleBufferRef)input_image_data;
-(void) addImage:(CMSampleBufferRef)input_image_data exposure:(float)exposureTime ISO:(float)ISO;
-(HDRImage) merge;
-(void) test;
@end
