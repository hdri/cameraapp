//
//  Utils.swift
//  AVCam
//
//  Created by a on 05/07/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

import Foundation
import UIKit

extension Float {
	func rounded(divisor: Float) -> Float {
		return (self * divisor).rounded() / divisor
	}
	
	func asCMTime() -> CMTime {
		return CMTimeMake(Int64(self * 1000000), 1000000)
	}
}

func enable(controls: [UIView], enable: Bool = true) {
	for control in controls {
		switch control {
		case let x as UIControl:
			x.isEnabled = enable
		case let x as UILabel:
			x.isEnabled = enable
		default:
			fatalError("unhandled case")
		}
	}
}

// https://stackoverflow.com/a/36111464
extension ClosedRange {
	func clamp(_ value : Bound) -> Bound {
		return self.lowerBound > value ? self.lowerBound
			: self.upperBound < value ? self.upperBound
			: value
	}
}
