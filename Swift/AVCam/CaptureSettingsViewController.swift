//
//  CaptureSettingsViewController.swift
//  
//
//  Created by a on 03/07/2018.
//

import UIKit

class CaptureSettingsViewController: UIViewController {
	@IBOutlet private weak var modeControl: UISegmentedControl!
	
	@IBOutlet private weak var measureButton: UIButton!
	
	@IBOutlet private weak var exposureTimeLabel: UILabel!
	@IBOutlet private weak var exposureTimeSlider: UISlider!
	
	@IBOutlet private weak var ISOLabel: UILabel!
	@IBOutlet private weak var ISOSlider: UISlider!
	
	@IBOutlet private weak var fixedISOSwitch: UISwitch!
	
	@IBOutlet private weak var EVStepSizeLabel: UILabel!
	@IBOutlet private weak var EVStepSizeSlider: UISlider!
	
	@IBOutlet private weak var EVStepsDownLabel: UILabel!
	@IBOutlet private weak var EVStepsDownSlider: UISlider!
	
	@IBOutlet private weak var EVStepsUpLabel: UILabel!
	@IBOutlet private weak var EVStepsUpSlider: UISlider!
	
	var ModeAuto: Bool {
		get {
			return modeControl.selectedSegmentIndex == 0
		}
		set {
			modeControl.selectedSegmentIndex = newValue ? 0 : 1
			modeControlValueChanged(modeControl)
		}
	}
	
	var ExposureTime: Float {
		get {
			return exposureTimeSlider.value
		}
		set {
			exposureTimeSlider.value = newValue
			exposureTimeValueChanged(exposureTimeSlider)
		}
	}
	
	var ISO: Float {
		get {
			return ISOSlider.value
		}
		set {
			ISOSlider.value = newValue
			ISOSliderValueChanged(ISOSlider)
		}
	}
	
	var FixedISO: Bool {
		get {
			return fixedISOSwitch.isOn
		}
		set {
			fixedISOSwitch.isOn = newValue
			// TODO: add ...ValueChanged handler?
		}
	}
	
	var EVStepSize: Float {
		get {
			return EVStepSizeSlider.value.rounded(divisor: 12) // 12 ... this allows 1/4 and 1/3 steps
		}
		set {
			EVStepSizeSlider.value = newValue
			EVStepSizeValueChanged(EVStepSizeSlider)
		}
	}
	
	var EVStepsDown: UInt {
		get {
			return UInt(abs(EVStepsDownSlider.value).rounded())
		}
		set {
			EVStepsDownSlider.value = -Float(newValue)
			EVStepsDownValueChanged(EVStepsDownSlider)
		}
	}
	
	var EVStepsUp: UInt {
		get {
			return UInt(EVStepsUpSlider.value.rounded())
		}
		set {
			EVStepsUpSlider.value = Float(newValue)
			EVStepsUpValueChanged(EVStepsUpSlider)
		}
	}

	var measureCallback : (() -> ())? = nil
	
    override func viewDidLoad() {
        super.viewDidLoad()

		manualControls = [measureButton, exposureTimeLabel, ISOLabel, exposureTimeSlider, ISOSlider]

		exposureTimeSlider.minimumValue = Float(CameraSupport.supportedExposureTime.lowerBound.seconds)
		exposureTimeSlider.maximumValue = Float(CameraSupport.supportedExposureTime.upperBound.seconds)
		ExposureTime = Float(CameraSupport.supportedExposureTime.lowerBound.seconds)

		ISOSlider.minimumValue = CameraSupport.supportedISO.lowerBound
		ISOSlider.maximumValue = CameraSupport.supportedISO.upperBound
		ISO = CameraSupport.supportedISO.lowerBound
		
		EVStepSizeValueChanged(EVStepSizeSlider)
		EVStepsUpValueChanged(EVStepsUpSlider)
		EVStepsDownValueChanged(EVStepsDownSlider)
    }
	
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@IBAction func measureClicked(_ sender: Any) {
		measureCallback!()
	}
	
	var manualControls : [UIView] = []
	
	@IBAction func modeControlValueChanged(_ sender: UISegmentedControl) {
		switch sender.titleForSegment(at: sender.selectedSegmentIndex)! { // TODO: compile-time check?
		case "Auto":
			// Switch to Auto -> measure, disable sliders
			enable(controls: manualControls, enable: false)

		case "Manual":
			// Switch to Manual -> enable sliders
			enable(controls: manualControls)
			
		default:
			fatalError("unknown case")
		}
	}
	
	@IBAction func exposureTimeValueChanged(_ sender: UISlider) {
		exposureTimeLabel.text = String(format: "Exposure time: %.6f", ExposureTime)
	}
	
	@IBAction func ISOSliderValueChanged(_ sender: UISlider) {
		ISOLabel.text = String(format: "ISO: %.0f", ISO)
	}
	
	@IBAction func EVStepSizeValueChanged(_ sender: UISlider) {
		EVStepSizeSlider.value = EVStepSize
		EVStepSizeLabel.text = String(format: "EV Step Size: %.3f", EVStepSize)
	}
	
	@IBAction func EVStepsDownValueChanged(_ sender: UISlider) {
		EVStepsDownSlider.value = -Float(EVStepsDown)
		EVStepsDownLabel.text = String(format: "EV Steps Down: %u", EVStepsDown)
	}
	
	@IBAction func EVStepsUpValueChanged(_ sender: UISlider) {
		EVStepsUpSlider.value = Float(EVStepsUp)
		EVStepsUpLabel.text = String(format: "EV Steps Up: %u", EVStepsUp)
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
