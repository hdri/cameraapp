/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	View controller for camera interface.
*/

import UIKit
import AVFoundation
import Photos

class CameraViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {
	
	@IBOutlet weak var settingsButton: UIButton!
	@IBOutlet weak var captureSettingsView: UIView!
	
	var captureSettingsViewController: CaptureSettingsViewController {
		return captureSettingsView.subviews.first?.next as! CaptureSettingsViewController
	}
	
	var merger : HDRMerge? = nil;
	
	func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
		// not capturing video
	}
	
	// MARK: View Controller Life Cycle
	
	func setBaseExposureParameters(exposureTime: Float, ISO: Float) {
		self.captureSettingsViewController.ExposureTime = exposureTime
		self.captureSettingsViewController.ISO = ISO
	}
	
    override func viewDidLoad() {
		super.viewDidLoad()
		
		let cv = OpenCVWrapper()
		print(cv.openCVVersionString())
		
		captureSettingsView.isHidden = true
		
		// Disable UI. The UI is enabled if and only if the session starts running.
		photoButton.isEnabled = false
		
		// Set up the video preview view.
		previewView.session = session
		
		captureSettingsViewController.measureCallback = {
			self.measureAutoExposure(measuringCompleted: { (exposureTime, ISO) in
				DispatchQueue.main.async {
					self.setBaseExposureParameters(exposureTime: exposureTime, ISO: ISO)
				}
			})
		}
		
		/*
			Check video authorization status. Video access is required and audio
			access is optional. If audio access is denied, audio is not recorded
			during movie recording.
		*/
		switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            case .authorized:
				// The user has previously granted access to the camera.
				break
			
			case .notDetermined:
				/*
					The user has not yet been presented with the option to grant
					video access. We suspend the session queue to delay session
					setup until the access request has completed.
				
					Note that audio access will be implicitly requested when we
					create an AVCaptureDeviceInput for audio during session setup.
				*/
				sessionQueue.suspend()
				AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [unowned self] granted in
					if !granted {
						self.setupResult = .notAuthorized
					}
					self.sessionQueue.resume()
				})
			
			default:
				// The user has previously denied access.
				setupResult = .notAuthorized
		}
		
		/*
			Setup the capture session.
			In general it is not safe to mutate an AVCaptureSession or any of its
			inputs, outputs, or connections from multiple threads at the same time.
		
			Why not do all of this on the main queue?
			Because AVCaptureSession.startRunning() is a blocking call which can
			take a long time. We dispatch session setup to the sessionQueue so
			that the main queue isn't blocked, which keeps the UI responsive.
		*/
		#if !((arch(i386) || arch(x86_64)) && os(iOS))
			// only if NOT running in simulator
		sessionQueue.async { [unowned self] in
			self.configureSession()
		}
		#endif
		
		//HDRMerge().test()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		sessionQueue.async {
			switch self.setupResult {
                case .success:
				    // Only setup observers and start the session running if setup succeeded.
                    self.addObservers()
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
				
                case .notAuthorized:
                    DispatchQueue.main.async { [unowned self] in
                        let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
				
                case .configurationFailed:
                    DispatchQueue.main.async { [unowned self] in
                        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		sessionQueue.async { [unowned self] in
			if self.setupResult == .success {
				self.session.stopRunning()
				self.isSessionRunning = self.session.isRunning
				self.removeObservers()
			}
		}
		
		super.viewWillDisappear(animated)
	}
	
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .all
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
			let deviceOrientation = UIDevice.current.orientation
			guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
				return
			}
			
			videoPreviewLayerConnection.videoOrientation = newVideoOrientation
		}
	}

	// MARK: Session Management
	
	private enum SessionSetupResult {
		case success
		case notAuthorized
		case configurationFailed
	}
	
	private let session = AVCaptureSession()
	
	private var isSessionRunning = false
	
	private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
	
	private var setupResult: SessionSetupResult = .success
	
	var videoDeviceInput: AVCaptureDeviceInput!
	
	@IBOutlet private weak var previewView: PreviewView!
	
	
	
	// Call this on the session queue.
	private func configureSession() {
		if setupResult != .success {
			return
		}
		
		session.beginConfiguration()
		
		/*
			We do not create an AVCaptureMovieFileOutput when setting up the session because the
			AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
		*/
		session.sessionPreset = AVCaptureSession.Preset.photo
		
		// Add video input.
		do {
			let defaultVideoDevice = CameraSupport.DefaultVideoDevice
			
			let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice!)
			
			if session.canAddInput(videoDeviceInput) {
				session.addInput(videoDeviceInput)
				self.videoDeviceInput = videoDeviceInput
				
				DispatchQueue.main.async {
					/*
						Why are we dispatching this to the main queue?
						Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
						can only be manipulated on the main thread.
						Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
						on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
					
						Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
						handled by CameraViewController.viewWillTransition(to:with:).
					*/
					let statusBarOrientation = UIApplication.shared.statusBarOrientation
					var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
					if statusBarOrientation != .unknown {
						if let videoOrientation = statusBarOrientation.videoOrientation {
							initialVideoOrientation = videoOrientation
						}
					}
					
					self.previewView.videoPreviewLayer.connection?.videoOrientation = initialVideoOrientation
				}
			}
			else {
				print("Could not add video device input to the session")
				setupResult = .configurationFailed
				session.commitConfiguration()
				return
			}
		}
		catch {
			print("Could not create video device input: \(error)")
			setupResult = .configurationFailed
			session.commitConfiguration()
			return
		}
		
		// Add photo output.
		if session.canAddOutput(photoOutput)
		{
			session.addOutput(photoOutput)
			
			photoOutput.isHighResolutionCaptureEnabled = true
			photoOutput.isLivePhotoCaptureEnabled = false
			
			print(photoOutput.availableRawPhotoPixelFormatTypes)
			print(photoOutput.availablePhotoPixelFormatTypes)
		}
		else {
			print("Could not add photo output to the session")
			setupResult = .configurationFailed
			session.commitConfiguration()
			return
		}
		
		session.commitConfiguration()
	}
	
	@IBAction private func resumeInterruptedSession(_ resumeButton: UIButton)
	{
		sessionQueue.async { [unowned self] in
			/*
				The session might fail to start running, e.g., if a phone or FaceTime call is still
				using audio or video. A failure to start the session running will be communicated via
				a session runtime error notification. To avoid repeatedly failing to start the session
				running, we only try to restart the session running in the session runtime error handler
				if we aren't trying to resume the session running.
			*/
			self.session.startRunning()
			self.isSessionRunning = self.session.isRunning
			if !self.session.isRunning {
				DispatchQueue.main.async { [unowned self] in
					let message = NSLocalizedString("Unable to resume", comment: "Alert message when unable to resume the session running")
					let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
					let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
					alertController.addAction(cancelAction)
					self.present(alertController, animated: true, completion: nil)
				}
			}
			else {
				DispatchQueue.main.async { [unowned self] in
					self.resumeButton.isHidden = true
				}
			}
		}
	}
	
	private enum CaptureMode: Int {
		case photo = 0
		case movie = 1
	}

	
	// MARK: Device Configuration
	
	@IBOutlet private weak var cameraUnavailableLabel: UILabel!
	
	private let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera, AVCaptureDevice.DeviceType.builtInDuoCamera], mediaType: AVMediaType.video, position: .unspecified)
	
	@IBAction private func changeCamera(_ cameraButton: UIButton) {
		cameraButton.isEnabled = false
		photoButton.isEnabled = false
		
		sessionQueue.async { [unowned self] in
			let currentVideoDevice = self.videoDeviceInput.device
			let currentPosition = currentVideoDevice.position
			
			let preferredPosition: AVCaptureDevice.Position
			let preferredDeviceType: AVCaptureDevice.DeviceType
			
			switch currentPosition {
				case .unspecified, .front:
					preferredPosition = .back
					preferredDeviceType = AVCaptureDevice.DeviceType.builtInDuoCamera
				
				case .back:
					preferredPosition = .front
					preferredDeviceType = AVCaptureDevice.DeviceType.builtInWideAngleCamera
			}
			
			let devices = self.videoDeviceDiscoverySession.devices
			var newVideoDevice: AVCaptureDevice? = nil
			
			// First, look for a device with both the preferred position and device type. Otherwise, look for a device with only the preferred position.
			if let device = devices.filter({ $0.position == preferredPosition && $0.deviceType == preferredDeviceType }).first {
				newVideoDevice = device
			}
			else if let device = devices.filter({ $0.position == preferredPosition }).first {
				newVideoDevice = device
			}

            if let videoDevice = newVideoDevice {
                do {
					let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
					
					self.session.beginConfiguration()
					
					// Remove the existing device input first, since using the front and back camera simultaneously is not supported.
					self.session.removeInput(self.videoDeviceInput)
					
					if self.session.canAddInput(videoDeviceInput) {
						NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentVideoDevice)
						
						NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
						
						self.session.addInput(videoDeviceInput)
						self.videoDeviceInput = videoDeviceInput
					}
					else {
						self.session.addInput(self.videoDeviceInput);
					}
					
					self.photoOutput.isLivePhotoCaptureEnabled = false
					
					self.session.commitConfiguration()
				}
				catch {
					print("Error occured while creating video device input: \(error)")
				}
			}
			
			DispatchQueue.main.async { [unowned self] in
				self.photoButton.isEnabled = true
			}
		}
	}
	
	@IBAction private func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer) {
		let devicePoint = self.previewView.videoPreviewLayer.captureDevicePointConverted(fromLayerPoint: gestureRecognizer.location(in: gestureRecognizer.view))
		focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
	}
	
	private func focus(with focusMode: AVCaptureDevice.FocusMode, exposureMode: AVCaptureDevice.ExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
		sessionQueue.async { [unowned self] in
			let device = self.videoDeviceInput.device
			do {
				try device.lockForConfiguration()
				
				/*
				Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
				Call set(Focus/Exposure)Mode() to apply the new point of interest.
				*/
				if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
					device.focusPointOfInterest = devicePoint
					device.focusMode = focusMode
				}
				
				if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
					device.exposurePointOfInterest = devicePoint
					device.exposureMode = exposureMode
				}
				
				device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
				device.unlockForConfiguration()
			}
			catch {
				print("Could not lock device for configuration: \(error)")
			}
		}
	}
	
	// MARK: Capturing Photos

	private let photoOutput = AVCapturePhotoOutput()
	
	private var inProgressPhotoCaptureDelegates : Set<PhotoCaptureDelegate> = []
	
	@IBOutlet private weak var photoButton: UIButton!
	
	@IBAction func showHideSettings(_ sender: Any) {
		captureSettingsView.isHidden = !captureSettingsView.isHidden
		settingsButton.isSelected = !captureSettingsView.isHidden
	}
	
	@IBAction private func photoButtonPressed(_ photoButton: UIButton) {
		photoButton.isEnabled = false
		
		let runCapture = {
			
			/*
			Retrieve the video preview layer's video orientation on the main queue before
			entering the session queue. We do this to ensure UI elements are accessed on
			the main thread and session configuration is done on the session queue.
			*/
			let videoPreviewLayerOrientation = self.previewView.videoPreviewLayer.connection?.videoOrientation
			
			// Update the photo output's connection to match the video orientation of the video preview layer.
			if let photoOutputConnection = self.photoOutput.connection(with: AVMediaType.video) {
				photoOutputConnection.videoOrientation = videoPreviewLayerOrientation!
			}
			
			var captureExposureSettings : [AVCaptureBracketedStillImageSettings] = []
			
			// "0 EV"
			let baseTime = self.captureSettingsViewController.ExposureTime
			let baseISO = self.captureSettingsViewController.ISO
			
			print("Using base exposure parameters: \(baseTime) s, ISO \(baseISO)")
			
			captureExposureSettings.append(AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: baseTime.asCMTime(), iso: baseISO))
			
			let stepSize = self.captureSettingsViewController.EVStepSize
			
			let generator = self.captureSettingsViewController.FixedISO ?
				AutoTimeExposureParametersGenerator(baseExposureTime: baseTime, baseISO: baseISO) :
				AutoTimeAndISOExposureParametersGenerator(baseExposureTime: baseTime, baseISO: baseISO)
			
			// "-x EV" - reduce ISO, then time
			let lowerLimit = Int(self.captureSettingsViewController.EVStepsDown)
			if lowerLimit > 0 {
				for	stepDown in 1...lowerLimit {
					let EVbias = -Float(stepDown) * stepSize
					let time, ISO : Float
					
					do {
						(time, ISO) = try generator.generate(forBias: EVbias)
					} catch ExposureParametersGeneratorError.outOfSupportedRange(let message) {
						print(message)
						print("breaking")
						break
					} catch {
						fatalError("unhandled error")
					}
					
					print("stepDown: \(stepDown): adding time \(time) ISO \(ISO)")
					
					let setting = AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: time.asCMTime(), iso: ISO)
					captureExposureSettings.append(setting)
				}
			}
			
			// "+x EV" - increase time up to 1/17 s, then start increasing ISO
			// "1/17" seems to be maximum when doing automatic exposure with bias "0 EV" (iPhone SE, iOS 10.2)
			let upperLimit = Int(self.captureSettingsViewController.EVStepsUp)
			if upperLimit > 0 {
				for	stepUp in 1...upperLimit {
					let EVbias = Float(stepUp) * stepSize
					let time, ISO : Float
					
					do {
						(time, ISO) = try generator.generate(forBias: EVbias)
					} catch ExposureParametersGeneratorError.outOfSupportedRange(let message) {
						print(message)
						print("breaking")
						break
					} catch {
						fatalError("unhandled error")
					}
					
					print("stepUp: \(stepUp): adding time \(time) ISO \(ISO)")
					
					let setting = AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: time.asCMTime(), iso: ISO)
					captureExposureSettings.append(setting)
				}
			}
			
			//print(captureExposureSettings)
			//print(captureExposureSettings.count)
			
			/*let captureExposureSettings : [AVCaptureBracketedStillImageSettings] = [
			AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(withExposureTargetBias: -6),
			AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(withExposureTargetBias: -3),
			AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(withExposureTargetBias: 0),
			AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(withExposureTargetBias: 3)
			]*/
			
			print(self.photoOutput.maxBracketedCapturePhotoCount)
			
			self.capturePhotos(captureExposureSettings: captureExposureSettings)
		}
		
		DispatchQueue.main.async {
			// If auto mode, measure the base exposure first, then capture
			if self.captureSettingsViewController.ModeAuto {
				self.measureAutoExposure(measuringCompleted: { (exposureTime, ISO) in
					// Update
					self.setBaseExposureParameters(exposureTime: exposureTime, ISO: ISO)
					runCapture()
				})
			} else {
				runCapture()
			}
		}
	}
	
	func capturePhotos(captureExposureSettings: [AVCaptureBracketedStillImageSettings]) {
		let captureCounter = CaptureCounter.Next()
		self.merger = HDRMerge(captureCounter)

		let captureLimit = self.photoOutput.maxBracketedCapturePhotoCount
		
		capture(exposureParametersList: captureExposureSettings, captureLimit: captureLimit)
	}
	
	// Take photo with the required exposure settings taking as-many-as-supported-by-hardware pictures (CaptureLimit = maxBracketedCapturePhotoCount) at the same time
	func capture(exposureParametersList: [AVCaptureBracketedStillImageSettings], captureLimit: Int) {
		
		if exposureParametersList.isEmpty {
			// TODO: call completion handler?
			return
		}
		
		let currentParameters = Array(exposureParametersList.prefix(captureLimit)) // First n
		let nextParameters = Array(exposureParametersList.dropFirst(captureLimit)) // The rest
		assert(exposureParametersList.count == currentParameters.count + nextParameters.count)
		
		//print(photoOutput.availablePhotoPixelFormatTypes)
		//let photoSettings = AVCapturePhotoBracketSettings.init(rawPixelFormatType: 0, processedFormat: [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA], bracketedSettings: currentExposureSettings)
		let photoSettings = AVCapturePhotoBracketSettings.init(rawPixelFormatType: 0, processedFormat: [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange], bracketedSettings: currentParameters)
		
		// Use a separate object for the photo capture delegate to isolate each capture life cycle.
		let photoCaptureDelegate = PhotoCaptureDelegate(
			willCapturePhotoAnimation: {
				DispatchQueue.main.async { [unowned self] in
					self.previewView.videoPreviewLayer.opacity = 0
					UIView.animate(withDuration: 0.25) { [unowned self] in
						self.previewView.videoPreviewLayer.opacity = 1
					}
				}
		},
			capturedPhoto: {sampleBuffer, exposureTime, ISO in
				self.sessionQueue.async {
					// TODO: do not keep in memory, just save (add to merger later, as a filename?)
					self.merger!.addImage(sampleBuffer, exposure: exposureTime, iso: ISO)
				}
		},
			completed: { [unowned self] photoCaptureDelegate in
				// When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
				self.sessionQueue.async { [unowned self] in
					self.inProgressPhotoCaptureDelegates.remove(photoCaptureDelegate)
					if nextParameters.isEmpty {
						print("all photos taken")
						self.merger!.merge()
						self.merger = nil
						DispatchQueue.main.async {
							self.photoButton.isEnabled = true
						}
					} else {
						print("\(nextParameters.count) photos left")
						self.capture(exposureParametersList: nextParameters, captureLimit: captureLimit)
					}
				}
			}
		)
		
		/*
		The Photo Output keeps a weak reference to the photo capture delegate so
		we store it in an array to maintain a strong reference to this object
		until the capture is completed.
		*/
		self.inProgressPhotoCaptureDelegates.insert(photoCaptureDelegate)
		
		
		// Tip: Capturing a multiple-image bracket may require allocation of additional resources. See the setPreparedPhotoSettingsArray(_:completionHandler:) method. - only reduces capture time
		// TODO: queue these requests to avoid crashes due to lack of memory when starting the capture of > ~13 photos at the same time
		self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureDelegate)
	}
	
	func measureAutoExposure(measuringCompleted: @escaping (Float, Float) -> ()) {
		// TODO: also get scene dynamic range (in another/similar function)?
		let captureExposureSettings : [AVCaptureBracketedStillImageSettings] = [
			AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(exposureTargetBias: 0)
		]
		
		let photoCaptureDelegate = PhotoCaptureDelegate(
			willCapturePhotoAnimation: {},
			capturedPhoto: {sampleBuffer, exposureTime, ISO in
				// TODO: capture more photos with different exposure values to determine scene's dynamic range
				//let stats = self.merger!.measureImage(sampleBuffer) - to determine whether it was clipped (black or white)

				measuringCompleted(exposureTime, ISO)
		},
			completed: {[unowned self] photoCaptureDelegate in
				self.inProgressPhotoCaptureDelegates.remove(photoCaptureDelegate)
		}
		)
		
		let photoSettings = AVCapturePhotoBracketSettings.init(rawPixelFormatType: 0, processedFormat: [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange], bracketedSettings: captureExposureSettings)
		
		self.inProgressPhotoCaptureDelegates.insert(photoCaptureDelegate)
		self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureDelegate)
	}
	
	@IBOutlet private weak var resumeButton: UIButton!
	
	// MARK: KVO and Notifications
	
	private var sessionRunningObserveContext = 0
	
	private func addObservers() {
		session.addObserver(self, forKeyPath: "running", options: .new, context: &sessionRunningObserveContext)
		
		NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: Notification.Name("AVCaptureSessionRuntimeErrorNotification"), object: session)
		
		/*
			A session can only run when the app is full screen. It will be interrupted
			in a multi-app layout, introduced in iOS 9, see also the documentation of
			AVCaptureSessionInterruptionReason. Add observers to handle these session
			interruptions and show a preview is paused message. See the documentation
			of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
		*/
		NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: Notification.Name("AVCaptureSessionWasInterruptedNotification"), object: session)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: Notification.Name("AVCaptureSessionInterruptionEndedNotification"), object: session)
	}
	
	private func removeObservers() {
		NotificationCenter.default.removeObserver(self)
		
		session.removeObserver(self, forKeyPath: "running", context: &sessionRunningObserveContext)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if context == &sessionRunningObserveContext {
			let newValue = change?[.newKey] as AnyObject?
			guard let isSessionRunning = newValue?.boolValue else { return }
			
			DispatchQueue.main.async { [unowned self] in
				self.photoButton.isEnabled = isSessionRunning
			}
		}
		else {
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
	@objc func subjectAreaDidChange(notification: NSNotification) {
		let devicePoint = CGPoint(x: 0.5, y: 0.5)
		focus(with: .autoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
	}
	
	@objc func sessionRuntimeError(notification: NSNotification) {
		guard let errorValue = notification.userInfo?[AVCaptureSessionErrorKey] as? NSError else {
			return
		}
		
        let error = AVError(_nsError: errorValue)
		print("Capture session runtime error: \(error)")
		
		/*
			Automatically try to restart the session running if media services were
			reset and the last start running succeeded. Otherwise, enable the user
			to try to resume the session running.
		*/
		if error.code == .mediaServicesWereReset {
			sessionQueue.async { [unowned self] in
				if self.isSessionRunning {
					self.session.startRunning()
					self.isSessionRunning = self.session.isRunning
				}
				else {
					DispatchQueue.main.async { [unowned self] in
						self.resumeButton.isHidden = false
					}
				}
			}
		}
		else {
            resumeButton.isHidden = false
		}
	}
	
	@objc func sessionWasInterrupted(notification: NSNotification) {
		/*
			In some scenarios we want to enable the user to resume the session running.
			For example, if music playback is initiated via control center while
			using AVCam, then the user can let AVCam resume
			the session running, which will stop music playback. Note that stopping
			music playback in control center will not automatically resume the session
			running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
		*/
		if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSession.InterruptionReason(rawValue: reasonIntegerValue) {
			print("Capture session was interrupted with reason \(reason)")
			
			var showResumeButton = false
			
			if reason == AVCaptureSession.InterruptionReason.audioDeviceInUseByAnotherClient || reason == AVCaptureSession.InterruptionReason.videoDeviceInUseByAnotherClient {
				showResumeButton = true
			}
			else if reason == AVCaptureSession.InterruptionReason.videoDeviceNotAvailableWithMultipleForegroundApps {
				// Simply fade-in a label to inform the user that the camera is unavailable.
				cameraUnavailableLabel.alpha = 0
				cameraUnavailableLabel.isHidden = false
				UIView.animate(withDuration: 0.25) { [unowned self] in
					self.cameraUnavailableLabel.alpha = 1
				}
			}
			
			if showResumeButton {
				// Simply fade-in a button to enable the user to try to resume the session running.
				resumeButton.alpha = 0
				resumeButton.isHidden = false
				UIView.animate(withDuration: 0.25) { [unowned self] in
					self.resumeButton.alpha = 1
				}
			}
		}
	}
	
	@objc func sessionInterruptionEnded(notification: NSNotification) {
		print("Capture session interruption ended")
		
		if !resumeButton.isHidden {
			UIView.animate(withDuration: 0.25,
				animations: { [unowned self] in
					self.resumeButton.alpha = 0
				}, completion: { [unowned self] finished in
					self.resumeButton.isHidden = true
				}
			)
		}
		if !cameraUnavailableLabel.isHidden {
			UIView.animate(withDuration: 0.25,
			    animations: { [unowned self] in
					self.cameraUnavailableLabel.alpha = 0
				}, completion: { [unowned self] finished in
					self.cameraUnavailableLabel.isHidden = true
				}
			)
		}
	}
}

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
            case .portrait: return .portrait
            case .portraitUpsideDown: return .portraitUpsideDown
            case .landscapeLeft: return .landscapeRight
            case .landscapeRight: return .landscapeLeft
            default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
            case .portrait: return .portrait
            case .portraitUpsideDown: return .portraitUpsideDown
            case .landscapeLeft: return .landscapeLeft
            case .landscapeRight: return .landscapeRight
            default: return nil
        }
    }
}

extension AVCaptureDevice.DiscoverySession {
	func uniqueDevicePositionsCount() -> Int {
		var uniqueDevicePositions = [AVCaptureDevice.Position]()
		
		for device in devices {
			if !uniqueDevicePositions.contains(device.position) {
				uniqueDevicePositions.append(device.position)
			}
		}
		
		return uniqueDevicePositions.count
	}
}
