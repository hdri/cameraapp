/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	Photo capture delegate.
*/

import AVFoundation
import Photos

class PhotoCaptureDelegate: NSObject, AVCapturePhotoCaptureDelegate {	
	private let willCapturePhotoAnimation: () -> ()
	
	private let capturedPhoto: (CMSampleBuffer, Float, Float) -> ()
	
	private let completed: (PhotoCaptureDelegate) -> ()

	private var photoData: [Data] = []
	
	init(willCapturePhotoAnimation: @escaping () -> (), capturedPhoto: @escaping (CMSampleBuffer, Float, Float) -> (), completed: @escaping (PhotoCaptureDelegate) -> ()) {
		self.willCapturePhotoAnimation = willCapturePhotoAnimation
		self.capturedPhoto = capturedPhoto
		self.completed = completed
	}
	
	private func didFinish() {
		completed(self)
	}
	
	func photoOutput(_ captureOutput: AVCapturePhotoOutput, willBeginCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
		//print("?")
	}
	
	func photoOutput(_ captureOutput: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
		willCapturePhotoAnimation()
	}
	
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
		
		//print(exposureTime)
		//print(ISO)
		if let photoSampleBuffer = photoSampleBuffer {
			let metadata = CMGetAttachment(photoSampleBuffer, "{Exif}" as CFString, nil) as! NSDictionary
			let exposureTime = (metadata.value(forKey: "ExposureTime") as! NSNumber).doubleValue
			let ISO = (metadata.value(forKey: "ISOSpeedRatings") as! NSArray).firstObject as! NSNumber
			
			//print(metadata)
			//print(exposureTime)
			//print(ISO)

			capturedPhoto(photoSampleBuffer, Float(exposureTime), Float(truncating: ISO))
		}
		else {
			print("Error capturing photo: \(error!)")
			return
		}
	}
	
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
		if let error = error {
			print("Error capturing photo: \(error)")
			didFinish()
			return
		}
		
		if photoData.isEmpty {
			print("No photo data resource")
			didFinish()
			return
		}
		
		PHPhotoLibrary.requestAuthorization { [unowned self] status in
			if status == .authorized {
				PHPhotoLibrary.shared().performChanges({ [unowned self] in
					for photoData in self.photoData {
						let creationRequest = PHAssetCreationRequest.forAsset()

						creationRequest.addResource(with: .photo, data: photoData, options: nil)
					}
					}, completionHandler: { [unowned self] success, error in
						if let error = error {
							print("Error occurered while saving photo to photo library: \(error)")
						}
						
						self.didFinish()
					}
				)
			} else {
				self.didFinish()
			}
		}
	}
}
