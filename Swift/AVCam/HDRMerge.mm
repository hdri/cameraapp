//
//  HDRMerge.m
//  AVCam
//
//  Created by a on 25/06/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#include <opencv2/opencv.hpp>

#include <fstream>

#import "HDRMerge.h"

#import "MergeRobertsonUnlimited.hpp"

#import "libHDRCNN/libHDRCNN.h"


// Adapted https://stackoverflow.com/a/41642300
cv::Mat convertImage(CMSampleBufferRef sampleBuffer)
{
	CVImageBufferRef cameraFrame = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(cameraFrame, 0);
	
	int w = (int)CVPixelBufferGetWidth(cameraFrame);
	int h = (int)CVPixelBufferGetHeight(cameraFrame);
	void *baseAddress = CVPixelBufferGetBaseAddressOfPlane(cameraFrame, 0);
	
	cv::Mat img_buffer(h+h/2, w, CV_8UC1, (uchar *)baseAddress);
	cv::Mat cam_frame;
	cv::cvtColor(img_buffer, cam_frame, cv::COLOR_YUV2RGB_NV21); // TODO: shouldn't this be "...2BGR"? but then the red and blue channels are swapped in the written image
	
	std::cout << "mat type: " << cam_frame.type() << std::endl;

	
	//cam_frame = cam_frame.t();
	
	//End processing
	CVPixelBufferUnlockBaseAddress( cameraFrame, 0 );
	
	return cam_frame;
}
/*cv::Mat convertImage(CMSampleBufferRef sampleBuffer) {
	CVImageBufferRef cameraFrame = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(cameraFrame, 0);
	
	int w = (int)CVPixelBufferGetWidth(cameraFrame);
	int h = (int)CVPixelBufferGetHeight(cameraFrame);
	void *baseAddress = CVPixelBufferGetBaseAddressOfPlane(cameraFrame, 0);
	
	cv::Mat img_buffer(h, w, CV_32FC4, (uchar *)baseAddress);
	
	cv::Mat cam_frame;
	cv::cvtColor(img_buffer, cam_frame, cv::COLOR_BGRA2BGR); // TODO: capture RAW and use cv::COLOR_BayerGB2BGR?

	std::cout << cv::imwrite([[[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject] URLByAppendingPathComponent:@"convertImageBGRA.hdr"] path] cString], cam_frame);

	cam_frame = cam_frame.t();
	
	//End processing
	CVPixelBufferUnlockBaseAddress( cameraFrame, 0 );
	
	return cam_frame;
}*/

struct CameraResponseCSVLine {
	int measured;
	float calibrated_r, calibrated_g, calibrated_b;
};
std::istream & operator>> (std::istream & is, CameraResponseCSVLine & t) {
	is >> t.measured;
	is.ignore(1, ',');
	is >> t.calibrated_r;
	is.ignore(1, ',');
	is >> t.calibrated_g;
	is.ignore(1, ',');
	is >> t.calibrated_b;
	return is;
}

static cv::Mat loadResponse(const char * fname) {
	std::ifstream response_csv(fname);
	if (!response_csv.good()) {
		throw std::invalid_argument("cannot open camera response file");
	}
	std::string header;
	response_csv >> header;
	if (header != "measured,calibrated_r,calibrated_g,calibrated_b") {
		throw std::invalid_argument("invalid camera response header");
	}
	
	cv::Mat response = cv::Mat(cv::LDR_SIZE, 1, CV_MAKETYPE(CV_32F, 3));
	for(int i = 0; i < cv::LDR_SIZE; i++) {
		CameraResponseCSVLine line;
		response_csv >> line;
		
		if (line.measured != i) {
			throw std::invalid_argument("invalid measured line number");
		}
		
		response.at<cv::Vec3f>(i) = cv::Vec3f(line.calibrated_r, line.calibrated_g, line.calibrated_b); // TODO: or BGR???
	}
	return response;
}

static int argmin(const std::vector<float> &exposure_times) {
	int min_i = -1;
	float min = std::numeric_limits<float>::max();
	for (int i = 0; i < exposure_times.size(); ++i) {
		if (exposure_times[i] < min) {
			min = exposure_times[i];
			min_i = i;
		}
	}
	assert(min_i >= 0);
	return min_i;
}

// Adapted from https://docs.opencv.org/master/d6/df5/group__photo__hdr.html
static cv::Mat merge(std::vector<cv::Mat> & images, // will be changed/cropped
					 const std::vector<float> & exposure_times,
					 const cv::Mat & camera_response) {
	
	// Align
	//std::vector<cv::Mat> aligned;
	cv::Ptr<cv::AlignMTB> align = cv::createAlignMTB();
	align->process(images, images, exposure_times, camera_response);
	
	//cv::Ptr<cv::CalibrateRobertson> calibrate = cv::createCalibrateRobertson();
	//calibrate->process(images, response, exposure_times);
	
	int shortest_exposure_i = argmin(exposure_times);
	
	cv::Mat & shortest_exposure_image = images.at(shortest_exposure_i);
	
	CV_Assert(shortest_exposure_image.depth() == CV_8U);
	CV_Assert(shortest_exposure_image.channels() == 3);
	//CV_Assert(shortest_exposure_image.isContinuous());

	if (!shortest_exposure_image.isContinuous()) {
		shortest_exposure_image = shortest_exposure_image.clone();
	}
	
	
	int width = shortest_exposure_image.size().width;
	int height = shortest_exposure_image.size().height;
	const uint8_t * LDR_ptr = shortest_exposure_image.ptr();
	
	cv::Mat reconstructed = cv::Mat(shortest_exposure_image.size(), CV_32FC3);
	float * reconstructed_ptr = reinterpret_cast<float *>(reconstructed.ptr());
	
	{
		NSLog(@"Reconstruction init");
		libHDRCNN * reconst = [[libHDRCNN alloc] init];
		NSLog(@"Reconstruction start");
		BOOL success = [reconst runInferenceOnImage:LDR_ptr width:width height:height channels:shortest_exposure_image.channels() output:reconstructed_ptr];
		
		NSLog(@"Reconstruction ended: %d", success);

		
	}
	
	
	
	shortest_exposure_image = reconstructed;
	
	cv::Mat hdr;
	cv::Ptr<cv::MergeRobertson> merger = cv::createMergeRobertsonUnlimited();
	merger->process(images, hdr, exposure_times, camera_response);

	// TODO: fix artifacts when photos do not fully cover the whole dynamic range of the scene
	
	return hdr;
	//imwrite("hdr.hdr", hdr);
	/*cv::Mat ldr;
	cv::Ptr<cv::TonemapDurand> tonemap = cv::createTonemapDurand(2.2f);
	tonemap->process(hdr, ldr);*/
	/*cv::Mat fusion;
	cv::Ptr<cv::MergeMertens> merge_mertens = cv::createMergeMertens();
	merge_mertens->process(images, fusion);*/
	//imwrite("fusion.png", fusion * 255);
	//imwrite("ldr.png", ldr * 255);
}

@implementation HDRMerge

//unsigned int captureCounter;
std::vector<cv::Mat> images;
std::vector<float> exposure_times;

-(id)init:(unsigned int)captureCounter {
	self = [super init];
	
	if (self) {
		self.captureCounter = captureCounter;
	}
	
	return self;
}

-(ImageStats)measureImage:(CMSampleBufferRef)input_image_data {
	cv::Mat frame = convertImage(input_image_data);

	ImageStats is;
	
	cv::minMaxLoc(frame, &is.min, &is.max);

	return is;
}

-(void)addImage:(CMSampleBufferRef)input_image_data exposure:(float)exposureTime ISO:(float)ISO {
	NSLog(@"addImage");
	cv::Mat frame = convertImage(input_image_data);
	images.push_back(frame);
	
	NSURL * documentsDir = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
	//NSMutableString *
	NSString * _Nullable fname = [[documentsDir URLByAppendingPathComponent:[NSString stringWithFormat:@"IMG_%u_%f_%.0f.png", self->_captureCounter, exposureTime, ISO]] path];
	cv::imwrite([fname cString], frame);

	exposure_times.push_back(exposureTime * ISO);
	NSLog(@"addImage end");
}

-(HDRImage)merge { // TODO: rename to "MergeAndFlush"
	//return HDRImage();
	// TODO: make merging more efficient - just load the just-needed image to RAM from disk
	NSLog(@"merge");

	// Load the camera response
	cv::Mat response = loadResponse([[[NSBundle mainBundle] pathForResource:@"camera_response_iPhone_SE" ofType:@"csv"] cString]); // TODO: load response based on current device (or camera chip)
	
	cv::Mat hdr = merge(images, exposure_times, response);
	
	NSURL * documentsDir = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
	NSString * name = [NSString stringWithFormat:@"IMG_%u_merged.hdr", self->_captureCounter];
	cv::imwrite([[[documentsDir URLByAppendingPathComponent:name] path] cString], hdr);
	
	// TODO: clear input images and exposure times?
	
	NSLog(@"merge end");

	return HDRImage(); // TODO: actually return the image instead of saving it
	//return HDRImage(merge(images, exposure_times));
}

-(void)test {
	//NSString * basePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"test_data"];
	NSString * basePath = [[NSBundle mainBundle] bundlePath];

	cv::Mat mat = cv::imread([[basePath stringByAppendingPathComponent:@"frame_0.012346.png"] cString]);
	
	images.push_back(mat);
	exposure_times.push_back(0.012346);
	
	images.push_back(cv::imread([[basePath stringByAppendingPathComponent:@"frame_0.037037.png"] cString]));
	exposure_times.push_back(0.037037);
	
	images.push_back(cv::imread([[basePath stringByAppendingPathComponent:@"frame_0.111111.png"] cString]));
	exposure_times.push_back(0.111111);
	
	images.push_back(cv::imread([[basePath stringByAppendingPathComponent:@"frame_0.333333.png"] cString]));
	exposure_times.push_back(0.333333);
	
	[self merge];
}

-(void)dealloc {
	images.clear();
	exposure_times.clear();	
}
@end
