//
//  CaptureCounter.swift
//  AVCam
//
//  Created by a on 04/07/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

import UIKit

class CaptureCounter: NSObject {
	private static let captureCounterKey = "captureCounter"
	
	static func Next() -> UInt32 {
		if UserDefaults.standard.object(forKey: captureCounterKey) == nil {
			UserDefaults.standard.setValue(0, forKey: captureCounterKey)
		}
		
		var counter = UserDefaults.standard.integer(forKey: captureCounterKey)
		counter += 1
		UserDefaults.standard.setValue(counter, forKey: captureCounterKey)
		return UInt32(counter)
	}
}
