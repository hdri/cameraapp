// https://bitbucket.org/hdri/mergerobertsonunlimited at b1c8a3ea206a14c17a62f1581a4f2a794bc3c6ea

#ifndef MergeRobertsonUnlimited_hpp
#define MergeRobertsonUnlimited_hpp

#include <opencv2/opencv.hpp>

namespace cv {
	
	class CV_EXPORTS_W MergeRobertsonUnlimited : public MergeExposures
	{
	public:
		CV_WRAP virtual void process(InputArrayOfArrays src, OutputArray dst,
									 InputArray times, InputArray response) = 0;
		CV_WRAP virtual void process(InputArrayOfArrays src, OutputArray dst, InputArray times) = 0;
	};
	
	
	CV_EXPORTS_W Ptr<MergeRobertson> createMergeRobertsonUnlimited();
	
}

#endif /* MergeRobertsonUnlimited_hpp */
