//
//  CameraSupport.swift
//  AVCam
//
//  Created by a on 03/07/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

import UIKit
import AVFoundation


class CameraSupport: NSObject {

	static let DefaultVideoDevice = CameraSupport.getDefaultVideoDevice()
	
	static func getDefaultVideoDevice() -> AVCaptureDevice? {
		// Choose the back dual camera if available, otherwise default to a wide angle camera.
		if let dualCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInDuoCamera, for: AVMediaType.video, position: .back) {
			return dualCameraDevice
		}
		else if let backCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: .back) {
			// If the back dual camera is not available, default to the back wide angle camera.
			return backCameraDevice
		}
		else if let frontCameraDevice = AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: .front) {
			// In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
			return frontCameraDevice
		}
		
		return nil
	}
	
	static let bestFormat = CameraSupport.getBestFormat()
	
	private static func getBestFormat() -> AVCaptureDevice.Format {
		var best : AVCaptureDevice.Format? = nil
		//var bestPixelCount : Int32 = 0
		
		/*for f in DefaultVideoDevice!.formats {
		print(f.formatDescription)
		print(CMFormatDescriptionGetMediaSubType(f.formatDescription))
		if CMFormatDescriptionGetMediaSubType(f.formatDescription) != 875704422 /* == "420f" */ {
		continue
		}
		
		let pixelCount = f.highResolutionStillImageDimensions.width * f.highResolutionStillImageDimensions.height
		
		if pixelCount > bestPixelCount {
		best = f
		bestPixelCount = pixelCount
		}
		}*/
		best = DefaultVideoDevice!.formats.last // HACK: better format selection (do not assume anything about the order of formats in the list)
		print("best format:", best.debugDescription)
		return best!
	}
	
	static var supportedISO: ClosedRange<Float> {
		get {
			return bestFormat.minISO ... bestFormat.maxISO
		}
	}
	
	static var supportedExposureTime: ClosedRange<CMTime> {
		get {
			return bestFormat.minExposureDuration ... bestFormat.maxExposureDuration
		}
	}
}
