#include "MergeRobertsonUnlimited.hpp"

namespace cv {
	
	float RobertsonUnlimitedWeight(int i)
	{
		Mat weight(LDR_SIZE, 1, CV_32FC3);
		const float q = (LDR_SIZE - 1) / 4.0f;
		const float e4 = exp(4.f);
		const float scale = e4/(e4 - 1.f);
		const float shift = 1 / (1.f - e4);
		
		float value = i / q - 2.0f;
		value = scale*exp(-value * value) + shift;
		
		return value;
	}
	
	Mat RobertsonUnlimitedWeights()
	{
		Mat weight(LDR_SIZE, 1, CV_32FC3);
		
		for(int i = 0; i < LDR_SIZE; i++) {
			weight.at<Vec3f>(i) = Vec3f::all(RobertsonUnlimitedWeight(i));
		}
		return weight;
	}
	
	void checkImageDimensionsUnlimited(const std::vector<Mat>& images)
	{
		CV_Assert(!images.empty());
		int width = images[0].cols;
		int height = images[0].rows;
		
		for(size_t i = 0; i < images.size(); i++) {
			CV_Assert(images[i].cols == width && images[i].rows == height);
		}
	}
	
	Mat linearResponseUnlimited(int channels)
	{
		Mat response = Mat(LDR_SIZE, 1, CV_MAKETYPE(CV_32F, channels));
		for(int i = 0; i < LDR_SIZE; i++) {
			response.at<Vec3f>(i) = Vec3f::all(static_cast<float>(i));
		}
		return response;
	}
	
	static Mat LUTUnlimited(const Mat & src, const cv::Mat &lut) {
		Mat w = cv::Mat(src.size(), CV_32FC3);
		
		for (int y = 0; y < src.size().height; ++y) {
			for (int x = 0; x < src.size().width; ++x) {
				for (int c = 0; c < src.channels(); ++c) {
					float value = src.at<cv::Vec3f>(y, x)[c] * 255;
					
					assert(value >= 0);
					assert(value <= 255);
					
					w.at<cv::Vec3f>(y, x)[c] = lut.at<Vec3f>(int(value))[c];
				}
			}
		}
		return w;
	}
	
	class MergeRobertsonUnlimitedImpl : public MergeRobertson
	{
	public:
		MergeRobertsonUnlimitedImpl() :
		name("MergeRobertson"),
		weight(RobertsonUnlimitedWeights())
		{
		}
		
		void process(InputArrayOfArrays src, OutputArray dst, InputArray _times, InputArray input_response)
		{
			//CV_INSTRUMENT_REGION()
			
			std::vector<Mat> images;
			src.getMatVector(images);
			Mat times = _times.getMat();
			
			CV_Assert(images.size() == times.total());
			checkImageDimensionsUnlimited(images);
			
			std::vector<bool> isHDR;
			isHDR.resize(images.size());
			
			for(size_t i = 0; i < images.size(); i++) {
				CV_Assert(images[i].channels() == images[0].channels());
				
				auto type = images[i].depth();
				CV_Assert(type == CV_8U || type == CV_32F);
				isHDR[i] = type == CV_32F;
			}
			
			int channels = images[0].channels();
			int CV_32FCC = CV_MAKETYPE(CV_32F, channels);
			
			dst.create(images[0].size(), CV_32FCC);
			Mat result = dst.getMat();
			
			Mat response = input_response.getMat();
			if(response.empty()) {
				float middle = LDR_SIZE / 2.0f;
				response = linearResponseUnlimited(channels) / middle; // 0..2
			}
			CV_Assert(response.rows == LDR_SIZE && response.cols == 1 &&
					  response.channels() == channels);
			
			result = Mat::zeros(images[0].size(), CV_32FCC);
			Mat wsum = Mat::zeros(images[0].size(), CV_32FCC);
			
			auto processImage = [&](const Mat & image, float time, bool skip_respnse_linearization = false) {
				std::stringstream ss;
				ss << "processImage_" << time << ".hdr";
				
				imwrite(ss.str(), image);
				
				Mat w = Mat(images[0].size(), CV_32FCC);
				Mat im = Mat(images[0].size(), CV_32FCC);
				LUT(image, weight, w); // prefer well-exposed pixels "in the middle" - near 128
				if (skip_respnse_linearization) {
					im = image;
				} else {
					LUT(image, response, im); // linearize
				}
				
				//auto t1 = im.type();
				//auto t2 = result.type();
				
				//assert(t1 == t2);
				
				Mat tmp;
				multiply(w, im, tmp, time, CV_32FCC);
				
				result += tmp;
				wsum += time * time * w;
			};
			
			for (size_t i = 0; i < images.size(); i++) {
				auto time = times.at<float>((int)i);
				
				if (isHDR[i]) {
					for (int EV = 0; EV > -5; EV -= 1) { // "Split" the HDR image into multiple LDR images
						// TODO: make "-= 1" infinitely small - integrate instead of summing?
						
						Mat simulatedImage = images[i].mul(255 * pow(2, EV)); // simulate exposure time

						simulatedImage = min(max(simulatedImage, 0), 255); // clamp values to interval <0, 255>
						simulatedImage.convertTo(simulatedImage, CV_8UC3); // convert to LDR
						
						processImage(simulatedImage, time * pow(2, EV), true /* do not linearize camera response - HDR image should be already linear */);
						
						double min;
						double max;
						minMaxLoc(simulatedImage, &min, &max);

						if (max < 255) {
							// There were no clipped pixels in this image - we do not need to simulate even shorter exposure times
							break;
						}
					}
				} else {
					// LDR
					processImage(images[i], time);
				}
			}
			
			result = result.mul(1 / wsum);
		}
		
		void process(InputArrayOfArrays src, OutputArray dst, InputArray times)
		{
			//CV_INSTRUMENT_REGION()
			
			process(src, dst, times, Mat());
		}
		
	protected:
		String name;
		Mat weight;
		
		
	};
	
	Ptr<MergeRobertson> createMergeRobertsonUnlimited()
	{
		return makePtr<MergeRobertsonUnlimitedImpl>();
	}
	
	
}
