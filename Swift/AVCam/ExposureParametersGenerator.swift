//
//  ExposureParametersGenerator.swift
//  AVCam
//
//  Created by a on 06/07/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

import Foundation

class ExposureParametersGenerator {
	let baseTime : Float
	let baseISO : Float
	
	let supportedISO = CameraSupport.supportedISO
	let supportedTime = Float(CameraSupport.supportedExposureTime.lowerBound.seconds)...Float(CameraSupport.supportedExposureTime.upperBound.seconds)
	
	init(baseExposureTime: Float, baseISO: Float) {
		self.baseTime = baseExposureTime
		self.baseISO = baseISO
	}
	
	func generate(forBias bias: Float) throws -> (Float, Float) {
		fatalError("This method must be overridden")
	}
}

class AutoTimeExposureParametersGenerator : ExposureParametersGenerator {
	override func generate(forBias bias: Float) throws -> (Float, Float) {
		let EVmultiplier = pow(2.0, bias)
		let baseExposure = baseTime * baseISO
		let targetExposure = baseExposure * EVmultiplier
		let time = targetExposure / baseISO
		
		if !supportedTime.contains(time) {
			throw ExposureParametersGeneratorError.outOfSupportedRange("AutoTimeExposureParametersGenerator cannot generate parameters for bias \(bias): time \(time) out of range for ISO \(baseISO)")
		}
		
		return (time, baseISO)
	}
}

class AutoTimeAndISOExposureParametersGenerator : ExposureParametersGenerator {
	override func generate(forBias bias: Float) throws -> (Float, Float) {
		let EVmultiplier = pow(2.0, bias)
		let baseExposure = baseTime * baseISO
		let targetExposure = baseExposure * EVmultiplier
		if bias < 0 {
			let ISO = supportedISO.clamp(baseISO * EVmultiplier)
			let time = targetExposure / ISO
			
			if !supportedTime.contains(time) {
				throw ExposureParametersGeneratorError.outOfSupportedRange("ExposureParametersGenerator cannot generate parameters for bias \(bias): time \(time) out of range for ISO \(ISO)")
			}

			return (time, ISO)
		} else {
			// bias >= 0
			let recommendedMaxTime = Float(1.0/17)
			let time = min(supportedTime.clamp(baseTime * EVmultiplier), recommendedMaxTime)
			let ISO = targetExposure / time
			
			if !supportedISO.contains(ISO) {
				// TODO: allow user to enable fall back to increasing time over the recommended limit (e.g. when on a tripod)
				throw ExposureParametersGeneratorError.outOfSupportedRange("ExposureParametersGenerator cannot generate parameters for bias \(bias): ISO \(ISO) out of range for time \(time)")
			}
			
			return (time, ISO)
		}
	}
}

enum ExposureParametersGeneratorError: Error {
	case outOfSupportedRange(String)
}

