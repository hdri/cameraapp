//
//  libHDRCNN.h
//  libHDRCNN
//
//  Created by a on 11/07/2018.
//  Copyright © 2018 Google. All rights reserved.
//

// git describe --long
// r1.8-16-g997cd04+

#import <Foundation/Foundation.h>


#include <memory>

namespace tensorflow {
	class Session;
}

@interface libHDRCNN : NSObject {
	std::unique_ptr<tensorflow::Session> session;
}

- (instancetype)init;

//-(BOOL)runInferenceOnImage:(float *)input width:(int)width height:(int)height output:(float *)output;
-(BOOL)runInferenceOnImage:(const uint8_t *)input_image_data width:(int)image_width height:(int)image_height channels:(int)image_channels output:(float *)output_image_data;

@end

