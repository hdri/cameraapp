//
//  OpenCVWrapper.m
//  AVCam
//
//  Created by a on 24/06/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#import <opencv2/opencv.hpp>

#import "OpenCVWrapper.h"

@implementation OpenCVWrapper

-(NSString *) openCVVersionString {
	return [NSString stringWithFormat:@"OpenCV Version %s", CV_VERSION];
}

@end
