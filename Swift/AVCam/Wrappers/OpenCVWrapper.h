//
//  OpenCVWrapper.h
//  AVCam
//
//  Created by a on 24/06/2018.
//  Copyright © 2018 Apple, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenCVWrapper : NSObject
	-(NSString *) openCVVersionString;
@end
