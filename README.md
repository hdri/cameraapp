# CameraApp
An HDR camera application for iOS, capturing specified dynamic range by taking multiple photos with different exposure parameters and merging them.

This application runs only on an actual device, either an iPad or iPhone, and cannot be run in Simulator.

## Downloading dependencies and opening the project
To fetch the required libraries, run the following command in the Swift folder (requires CocoaPods installed):

	pods install
	
Then open the file "AVCam Swift.xcworkspace" in Xcode. Editing/building/running the project follows standard iOS application development procedures.

## Requirements

### Build

Xcode 8.0, iOS 10.0 SDK

### Runtime

iOS 10.0 or later

## Changes from Previous Version

- Adopt AVCapturePhotoOutput
- Capture Live Photos
- Add privacy keys to Info.plist
- Add a version of AVCam in Swift 3
- Remove support for AVCaptureStillImageOutput
- Bug fixes


It is based on the "AVCam-iOS: Using AVFoundation to Capture Photos and Movies" codebase.
Copyright (C) 2016 Apple Inc. All rights reserved.
